package com.warehouse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.warehouse.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long>{
	
	List<Product> findAll();
	Product findOneById(Long id);

}
