package com.warehouse.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.warehouse.model.StoreUnit;

public interface StoreUnitRepository extends CrudRepository<StoreUnit, Long>{

	List<StoreUnit> findAll();
	
	List<StoreUnit> findAllByEmptyLike(boolean empty);
	StoreUnit findOneById(Long id);
}
