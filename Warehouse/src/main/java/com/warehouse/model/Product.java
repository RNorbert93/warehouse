package com.warehouse.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Product {

	@GeneratedValue
	@Id
	private long id;
	
	private String name;
	private String owner;
	private String storeUnitType;
	
	@OneToOne
	private StoreUnit whichStoreUnit;
	
	private Product() {
		
	}
	


	public Product(long id, String name, String owner) {
		super();
		this.id = id;
		this.name = name;
		this.owner = owner;
	}
	
	




	public Product(String name, String owner, String storeUnitType, StoreUnit whichStoreUnit) {
		super();
		this.name = name;
		this.owner = owner;
		this.storeUnitType = storeUnitType;
		this.whichStoreUnit = whichStoreUnit;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public StoreUnit getWhichStoreUnit() {
		return whichStoreUnit;
	}

	public void setWhichStoreUnit(StoreUnit whichStoreUnit) {
		this.whichStoreUnit = whichStoreUnit;
	}



	public String getStoreUnitType() {
		return storeUnitType;
	}



	public void setStoreUnitType(String storeUnitType) {
		this.storeUnitType = storeUnitType;
	}
	


}
