package com.warehouse.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class StoreUnit {

	@GeneratedValue
	@Id
	private long id;
	private boolean empty;
	private String storeUnitType;
	
	@OneToOne
	private Product storedProduct;
	
	private StoreUnit() {
		
	}
	
	

	public StoreUnit(long id, boolean empty, String storeUnitType) {
		super();
		this.id = id;
		this.empty = empty;
		this.storeUnitType = storeUnitType;
	}
	




	public StoreUnit(String storeUnitType) {
		super();
		this.storeUnitType = storeUnitType;
		this.empty = true;
		this.storedProduct = null;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isEmpty() {
		return empty;
	}

	public void setEmpty(boolean empty) {
		this.empty = empty;
	}

	public Product getStoredProduct() {
		return storedProduct;
	}

	public void setStoredProduct(Product storedProduct) {
		this.storedProduct = storedProduct;
	}

	public String getStoreUnitType() {
		return storeUnitType;
	}

	public void setStoreUnitType(String storeUnitType) {
		this.storeUnitType = storeUnitType;
	}

	
	

}
