package com.warehouse.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class ProductForm {

	@NotEmpty @Size(max = 64)
	private String name;
	@NotEmpty @Size(max = 64)
	private String owner;
	@NotEmpty @Size(max = 16)
	private String storeUnitType;
	private Long storeUnitId;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getStoreUnitType() {
		return storeUnitType;
	}
	public void setStoreUnitType(String storeUnitType) {
		this.storeUnitType = storeUnitType;
	}
	public Long getStoreUnitId() {
		return storeUnitId;
	}
	public void setStoreUnitId(Long storeUnitId) {
		this.storeUnitId = storeUnitId;
	}

	

}
