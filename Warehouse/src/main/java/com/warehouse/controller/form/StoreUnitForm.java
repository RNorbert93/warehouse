package com.warehouse.controller.form;

public class StoreUnitForm {

	private String storeUnitType;
	private int numberOfNewStoreUnits;
	
	public String getStoreUnitType() {
		return storeUnitType;
	}
	public void setStoreUnitType(String storeUnitType) {
		this.storeUnitType = storeUnitType;
	}
	public int getNumberOfNewStoreUnits() {
		return numberOfNewStoreUnits;
	}
	public void setNumberOfNewStoreUnits(int numberOfNewStoreUnits) {
		this.numberOfNewStoreUnits = numberOfNewStoreUnits;
	}
	

}
