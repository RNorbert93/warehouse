package com.warehouse.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.warehouse.controller.form.ProductForm;
import com.warehouse.model.Product;
import com.warehouse.model.StoreUnit;
import com.warehouse.service.ProductService;
import com.warehouse.service.StoreUnitService;

@Controller
@RequestMapping("/new-product.html")
public class NewProductController {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	StoreUnitService SUService;
	
	@Autowired
	ProductService ProdService;
	
	List<StoreUnit> storeUnits;

	@RequestMapping(method = RequestMethod.GET)
	public String doGet(Model model) {
		storeUnits = SUService.getStoreUnitsByEmpty(true);
		if(storeUnits.isEmpty()) {
			model.addAttribute("msg", "ures");
		}
		model.addAttribute("storeUnits", storeUnits);
		return "new-product";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String greetTheUser(Model model, ProductForm form) {
		
		ProdService.createNewProduct(new Product(form.getName(), form.getOwner(), form.getStoreUnitType(), SUService.getStoreUnitById(form.getStoreUnitId())));
		logger.info("New product has been created: #{}, #{}, #{}", form.getName(), form.getOwner(), form.getStoreUnitType());
		
		storeUnits = SUService.getStoreUnitsByEmpty(true);
		if(storeUnits.isEmpty()) {
			model.addAttribute("msg", "ures");
		}
		model.addAttribute("storeUnits", storeUnits);
		return "new-product";
	}
}
