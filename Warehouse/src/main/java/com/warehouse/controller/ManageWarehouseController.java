package com.warehouse.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.warehouse.controller.form.DeleteStoreUnitForm;
import com.warehouse.controller.form.StoreUnitForm;
import com.warehouse.service.StoreUnitService;

@Controller
@RequestMapping("/manage-warehouse.html")
public class ManageWarehouseController {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	StoreUnitService SUService;
	
	@Autowired
	public void setProductService(StoreUnitService SUService) {
		this.SUService = SUService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String doGet(Model model, StoreUnitForm storeUnitForm, DeleteStoreUnitForm deleteStoreUnitForm) {
		
		if(storeUnitForm.getStoreUnitType() != null) {
			SUService.createNewStoreUnits(storeUnitForm.getStoreUnitType(), storeUnitForm.getNumberOfNewStoreUnits());
			logger.info("#{} StoreUnit has been created, their type: #{}", storeUnitForm.getNumberOfNewStoreUnits(), storeUnitForm.getStoreUnitType());
		} else if(deleteStoreUnitForm.getId() != null) {
			SUService.deleteStoreUnitById(deleteStoreUnitForm.getId());
			logger.info("StoreUnit #{} has been deleted", deleteStoreUnitForm.getId());
		}
		
		model.addAttribute("storeUnits", SUService.getStoreUnitsByEmpty(true));
		return "manage-warehouse";
	}
}
