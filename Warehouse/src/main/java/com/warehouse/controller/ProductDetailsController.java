package com.warehouse.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.warehouse.service.ProductService;

@Controller
@RequestMapping("/product-details.html")
public class ProductDetailsController {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	ProductService prodService;

	@RequestMapping(method = RequestMethod.GET)
	public String doGet(Model model, @RequestParam Long productId, @RequestParam(required = false) boolean delete) {
		
		if(delete) {
			model.addAttribute("msg", "success");
			prodService.deleteProductById(productId);
			logger.info("product #{} has been deleted", productId);
		} else {
		model.addAttribute("product", prodService.getProductById(productId));
		}
		
		return "product-details";
	}
}
