package com.warehouse.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.warehouse.service.StoreUnitService;

@Controller
public class HomeController {
	
	@Autowired
	StoreUnitService SUService;

	@RequestMapping(value = {"/index.html", "/"})
	public String displayMenu(){
		return "index";
	}
	
	@PostConstruct
	public void init() {
		SUService.init();
	}
}
