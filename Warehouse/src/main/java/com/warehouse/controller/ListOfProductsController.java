package com.warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.warehouse.service.ProductService;

@Controller
@RequestMapping("/list-of-products.html")
public class ListOfProductsController {
	
	@Autowired
	ProductService prodService;

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("products",prodService.getProducts());
		return "list-of-products";
	}
}
