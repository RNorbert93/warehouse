package com.warehouse.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.warehouse.model.Product;
import com.warehouse.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	ProductRepository prodRepo;
	
	public List<Product> getProducts(){
		return prodRepo.findAll();
	}
	
	public Product getProductById(Long id) {
		return prodRepo.findOneById(id);
	}
	
	public void createNewProduct(Product product) {
		prodRepo.save(product);
	}
	
	public void deleteProductById(Long id) {
		Product product = prodRepo.findOneById(id);
		product.getWhichStoreUnit().setEmpty(true);
		prodRepo.delete(product);
	}

}
