package com.warehouse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.warehouse.model.StoreUnit;
import com.warehouse.repository.StoreUnitRepository;

@Service
public class StoreUnitService {

	StoreUnitRepository SURepo;

	@Autowired
	public void setSUrepo(StoreUnitRepository SURepo) {
		this.SURepo = SURepo;
	}

	public List<StoreUnit> getStoreUnitsByEmpty(boolean empty){
		return SURepo.findAllByEmptyLike(empty);
	}
	
	public StoreUnit getStoreUnitById(Long id) {
		StoreUnit storeUnit = SURepo.findOneById(id);
		storeUnit.setEmpty(false);
		return storeUnit;
	}
	
	public void createNewStoreUnits(String storeUnitType, int numberOfNewStoreUnits) {
		for(int i=0; i<numberOfNewStoreUnits; i++) {
			SURepo.save(new StoreUnit(storeUnitType));
		}
	}
	
	public void deleteStoreUnitById(Long id) {
		SURepo.deleteById(id);
	}
	
	public void init() {
		SURepo.save(new StoreUnit("raklap"));
		SURepo.save(new StoreUnit("raklap"));
		SURepo.save(new StoreUnit("raklap"));
		SURepo.save(new StoreUnit("kontener"));
		SURepo.save(new StoreUnit("kontener"));
	}
	
}
